
// tslint:disable

export const mapMarker: any = {
    path: "M16 0c-5.523 0-10 4.477-10 10 0 10 10 22 10 22s10-12 10-22c0-5.523-4.477-10-10-10zM16 16c-3.314 0-6-2.686-6-6s2.686-6 6-6 6 2.686 6 6-2.686 6-6 6z",
    fillColor: '#FF0000',
    fillOpacity: .6,
    anchor: new google.maps.Point(16, 32),
    strokeWeight: 0,
    scale: 1
}

export const directionsOptions = {
    suppressMarkers: true,
    suppressInfoWindows: true,
    preserveViewport: true,
    draggable: true,
    polylineOptions: {
      strokeColor: '#1565c0',
    }
  };


export const mapStyles: google.maps.MapTypeStyle[] = [
    {
        featureType: 'transit',
        stylers: [
            { visibility: 'off' }
        ]
    },
    {
        featureType: 'poi.medical',
        stylers: [
            { visibility: 'off' }
        ]
    },
    {
        featureType: 'poi.school',
        stylers: [
            { visibility: 'off' }
        ]
    },
    {
        featureType: 'poi.business',
        stylers: [
            { visibility: 'off' }
        ]
    },
    {
        featureType: 'road.arterial',
        elementType: 'labels',
        stylers: [
            { visibility: 'off' }
        ]
    },
    {
        featureType: 'road.highway',
        stylers: [
            { visibility: 'on' }
        ]
    },
    {
        featureType: 'road.highway',
        elementType: 'labels',
        stylers: [
            { visibility: 'simplified' }
        ]
    },
    {
        featureType: 'road.local',
        elementType: 'labels',
        stylers: [
            { visibility: 'off' }
        ]
    },
];

