import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private _auth: AuthService, private _router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let obs$;
    if (this._auth.token && !req.url.includes('api.flickr.com') && !req.url.includes('google.com')) {
      const authReq = req.clone({
        setHeaders: { authorization: 'bearer ' + this._auth.token }
      });
      obs$ = next.handle(authReq);
    } else
      obs$ = next.handle(req);

    return obs$.pipe(tap(null, error => {
      if (error instanceof HttpErrorResponse && error.status === 401) {
        this._router.navigateByUrl('/login');
      }
      console.log('HTTP Error intercepted');
    }),
    );
  }
}
