import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { MapEditorComponent } from '../map-editor/map-editor.component';
import { MapDrawerComponent } from '../map-drawer/map-drawer.component';
import { AuthGuard } from '../guards/auth.guard';

export const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'map', component: MapDrawerComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];
