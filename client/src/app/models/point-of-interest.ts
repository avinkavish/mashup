export interface PointOfInterest {
  id: string;
  name: string;
  perex: string;
  marker: string;
  thumbnail_url: string;
  photos: string[];
  location: google.maps.LatLngLiteral;
}
