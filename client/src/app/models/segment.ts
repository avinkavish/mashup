import { addSeconds, format, closestIndexTo } from 'date-fns';
import { EmbeddedViewRef } from '@angular/core';
import * as SnazzyInfoWindow from 'snazzy-info-window';

/*
* Models a single leg of the journey
* Stores a marker and directions from the previous marker to this
* Saves current weather and weather forecast
*/
export class Segment {
  place = 'no name';
  marker: google.maps.Marker;
  drawnRoute?: google.maps.Polyline;
  directionsRenderer?: google.maps.DirectionsRenderer;
  totalDistance = 0; // in meters
  totalDuration = 0; // in seconds
  weather?: WeatherData;
  viewRef: EmbeddedViewRef<Segment>;
  photos?: string[];
  infoWindow: SnazzyInfoWindow;

  /* Makes a segment out of freshly parsed JSON */
  public static fromJSON(lit: any): Segment {
    const s = new Segment(new google.maps.Marker({
      position: lit.marker
    }));
    s.place = lit.place;
    s.directionsRenderer = lit.directionsResult ? new google.maps.DirectionsRenderer({
      directions: lit.directionsResult,
    }) : null;
    s.totalDistance = lit.totalDist;
    s.totalDuration = lit.totalDur;
    s.weather = lit.weather;
    s.viewRef = null;
    s.photos = lit.photos;

    return s;
  }

  public constructor(marker: google.maps.Marker) {
    this.marker = marker;
  }

  // Note: there will ever only be one route and one leg because we manage routes and legs internaly
  public getDistance(): number {
    if (this.directionsRenderer)
      return this.directionsRenderer.getDirections().routes[0].legs[0].distance.value;
    else
      return 0;
  }

  public getDuration(): number {
    if (this.directionsRenderer)
      return this.directionsRenderer.getDirections().routes[0].legs[0].duration.value;
    else
      return 0;
  }

  public getETA = () => addSeconds(new Date(), this.totalDuration);

  public getDistLiteral = () => (this.getDistance() / 1000).toFixed(1) + ' km';

  public getTotalDistLiteral = () => (this.totalDistance / 1000).toFixed(1) + ' km';

  public getDurLiteral() {
    const secs = this.getDuration();
    return `${(secs / 3600).toFixed(0)} hours ${((secs / 60) % 60).toFixed(0)} mins`;
  }

  public getTotalDurLiteral() {
    const secs = this.totalDuration;
    const hours = Math.floor(secs / 3600);
    return `${hours} hours ${(Math.floor(secs / 60) % 60).toFixed(0)} mins`;
  }

  /* Weather forecast based on ETA */
  public getForecast() {
    if (!this.weather || !this.weather.forecast) return null;
    const timestamp = (this.getETA().getTime() / 1000);
    let index = 0;
    this.weather.forecast.reduce((prev, curr, i) => {
      const diff = Math.abs(curr.timestamp - timestamp);
      if (prev < diff)
        return prev;

      index = i;
      return diff;

    }, Infinity);

    return this.weather.forecast[index];
  }

  /* Strip all Google Maps DOM objects and create pure JSON for storage */
  public toJSON(): object {
    return {
      directionsResult: this.directionsRenderer ? this.directionsRenderer.getDirections() : null,
      place: this.place,
      marker: this.marker ? this.marker.getPosition().toJSON() : null,
      totalDist: this.totalDistance,
      totalDur: this.totalDuration,
      weather: this.weather,
      photos: this.photos
    };
  }

  /* Destroy all DOM references */
  public destroy() {
    this.marker.setMap(null);
    this.marker.unbindAll();
    if (this.directionsRenderer) {
      this.directionsRenderer.setMap(null);
      this.directionsRenderer.unbindAll();
    }
    if (this.drawnRoute) {
      this.drawnRoute.setMap(null);
      this.drawnRoute.unbindAll();
    }
    // tslint:disable-next-line:no-unused-expression
    this.infoWindow && this.infoWindow.destroy();
    this.viewRef.destroy();
  }
}

/* Interfaces for typesafety */
export interface WeatherData {
  current: WeatherInfo;
  forecast: WeatherInfo[];
}

export interface WeatherInfo {
  timestamp?: number;
  station?: string;
  temp: number;
  desc: string;
  icon: string;
}

export interface TextValuePair {
  text: string;
  value: number;
}
