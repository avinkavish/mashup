import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { SEGMENT_DATA } from '../custom/tokens';
import { Segment } from '../models/segment';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  public hires: string[];
  @ViewChild('slider') slider: ElementRef;
  left = 0;

  constructor(@Inject(SEGMENT_DATA) public segment: Segment) {
    this.hires = segment.photos.map(v => v.replace('n.jpg', 'h.jpg'));
  }

  ngOnInit() {
  }

  slideLeft() {
    const slider = this.slider.nativeElement as HTMLElement;
    if (this.left >= 0) return;
    this.left += 10;
    slider.style.transform = `translateX(${this.left}%)`;
  }

  slideRight() {
    const slider = this.slider.nativeElement as HTMLElement;
    if (this.left <= -90) return;
    this.left -= 10;
    slider.style.transform = `translateX(${this.left}%)`;
  }

}
