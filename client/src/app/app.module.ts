import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { OverlayDirective } from './directives/overlay.directive';
import {
  MatButtonToggleModule, MatIconModule, MatTabsModule, MatSidenavModule,
  MatButtonModule, MatCardModule, MatSnackBarModule, MatToolbarModule, MatTooltipModule, MatInputModule
} from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { InfoComponent } from './info/info.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { MapEditorComponent } from './map-editor/map-editor.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { routes } from './models/routes';
import { MapDrawerComponent } from './map-drawer/map-drawer.component';
import { TokenInterceptor } from './interceptors/token-interceptor';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';

@NgModule({
  declarations: [
    AppComponent,
    OverlayDirective,
    InfoComponent,
    MapEditorComponent,
    SidebarComponent,
    LoginComponent,
    MapDrawerComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonToggleModule,
    MatIconModule,
    MatTabsModule,
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    FormsModule,
    MatToolbarModule,
    OverlayModule,
    MatTooltipModule,
    MatInputModule,
    NgProgressModule.forRoot(),
    NgProgressHttpModule.forRoot()
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }],
  bootstrap: [AppComponent],
  entryComponents: [InfoComponent]
})
export class AppModule { }
