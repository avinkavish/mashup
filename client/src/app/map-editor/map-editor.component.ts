import {
  Component, AfterViewInit, ChangeDetectionStrategy,
  ViewChild, TemplateRef, ViewContainerRef, ElementRef, OnDestroy, HostListener, ChangeDetectorRef
} from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { MatSnackBar, MatButtonToggleChange } from '@angular/material';
import { Segment } from '../models/segment';
import { MapsService } from '../services/maps.service';
import { fromEvent, zip, fromEventPattern, Observable, Subscription, merge, of } from 'rxjs';
import { FlickrService } from '../services/flickr.service';
import * as SnazzyInfoWindow from 'snazzy-info-window';
import { OverlayRef } from '@angular/cdk/overlay';
import { mapMarker } from '../../styles/mapStyles';
import { POIService } from '../services/poi.service';
import { DrawingService } from '../services/drawing.service';
import { SidebarService } from '../services/sidebar.service';
import { debounceTime, switchMap, distinctUntilChanged, distinctUntilKeyChanged, map, tap } from 'rxjs/operators';
import { StateManager } from '../services/state.manager';
import { mapStyleWhite } from '../../styles/mapStyleWhite';
import { distanceInWordsToNow } from 'date-fns';
import { OverlayService } from '../services/overlay.service';
import { AuthService } from '../services/auth.service';
import { mapStylesLight } from '../../styles/mapStylesLight';
import { LoggingService } from '../services/logging.service';

@Component({
  selector: 'app-map-editor',
  templateUrl: './map-editor.component.html',
  styleUrls: ['./map-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush // Change detection is only performed when new values are passed by reference
})
export class MapEditorComponent implements AfterViewInit, OnDestroy {
  title = 'mashup';
  overlayRef: OverlayRef;
  clearPlan = false;
  darkTheme = true;

  get tool(): string {
    return this._tool;
  }

  set tool(val: string) {
    if (val !== this._tool) {
      this._tool = val;
      this.changeTool(val);
    }
  }
  private _tool = 'pan';

  get mapView() {
    return this._mapView;
  }

  set mapView(val: string) {
    if (val !== this._mapView) {
      this._mapView = val;
      this.changeView(val);
    }
  }
  private _mapView = 'poi';
  segments$: Observable<Segment[]>;

  get currentStart(): boolean {
    return this._currentStart;
  }

  set currentStart(val: boolean) {
    if (val !== this._currentStart) {
      this._currentStart = val;
      this.startAtCurrentLocation();
    }
  }

  _currentStart = false;
  private subscriptions: Subscription = new Subscription();
  private snackBarProps = { duration: 3500, panelClass: !this.darkTheme ? 'snackbar-white' : '' };

  @ViewChild('map') mapRef: ElementRef;
  @ViewChild('weatherTemplate') template: TemplateRef<any>;

  constructor(private snackBar: MatSnackBar, private weather: WeatherService, private flickr: FlickrService, public sidebar: SidebarService,
    private _mapsService: MapsService, private drawingService: DrawingService, private poi: POIService, private overlay: OverlayService,
    private _stateManager: StateManager, private vcf: ViewContainerRef, private _cd: ChangeDetectorRef, private _auth: AuthService,
    private _log: LoggingService) {
    this.segments$ = this._stateManager.observeSegments();
  }

  initGMaps() {
    const mapDiv = document.getElementById('map');
    if (!mapDiv) {
      console.log('Failed to get div for map insertion');
      return;
    }
    const gMap = this._mapsService.initMap(mapDiv);
    const drawingManager = this.drawingService.init(gMap);
    const lineComplete$ = this.gMapsEventToObservable(drawingManager, 'polylinecomplete');
    const markerAdded$ = this.gMapsEventToObservable(drawingManager, 'markercomplete');
    const boundsChanged$ = this.gMapsEventToObservable(gMap, 'bounds_changed');

    const lineCompleteSub = lineComplete$.subscribe((e: google.maps.Polyline) => this.snapToRoad(e));
    const markerAddedSub = markerAdded$.subscribe((e: google.maps.Marker) => this.markerAdded(e));

    // Debounce time to ensure that excessive API calls aren't made
    const boundsChangedSub = boundsChanged$.pipe(
      debounceTime(1000),
      switchMap(() => this.poi.displayPOI(gMap.getBounds()))
    ).subscribe();

    this.subscriptions.add(lineCompleteSub);
    this.subscriptions.add(markerAddedSub);
    this.subscriptions.add(boundsChangedSub);
    this.addKbListeners();
  }

  markerAdded(marker: google.maps.Marker, unshift = false) {
    const segments = this._stateManager.select() as Segment[];
    const segment: Segment = new Segment(marker);
    const point = marker.getPosition();
    const infoWindow = this.createInfoWindow(segment);

    let route$: Observable<string> | Observable<google.maps.DirectionsRenderer> = of('noop');
    if (segments.length !== 0) {
      let destination, origin;
      if (unshift) {
        destination = segments[1].marker;
        origin = marker;
      } else {
        destination = marker;
        origin = segments[segments.length - 1].marker;
      }
      route$ = this._mapsService.route(origin, destination);
      route$.subscribe(result => {
        segment.directionsRenderer = result;
        segment.totalDistance = segments.reduce((tot, curr) => tot += curr.getDistance(), 0) + segment.getDistance();
        segment.totalDuration = segments.reduce((tot, curr) => tot += curr.getDuration(), 0) + segment.getDuration();
      });
    }
    marker.setLabel((this._stateManager.select('count') as number + 1).toString());
    this._stateManager.addSegment(segment);

    // Pops up at bottom, for undoing
    const snackBarRef = this.snackBar.open('Marker Added', 'Undo', this.snackBarProps);
    this.subscriptions.add(snackBarRef.onAction().subscribe(() => this.removeMarker(segment)));

    const weather$ = this.weather.get(point);
    const flickr$ = this.flickr.getByLocation(marker.getPosition().toJSON());
    const geocode$ = this._mapsService.geocodePlace(marker);
    weather$.subscribe(res => segment.weather = res);
    flickr$.subscribe((res: any) => segment.photos = res, null, () => infoWindow.open());
    geocode$.subscribe(res => segment.place = res);

    merge(
      weather$,
      flickr$,
      geocode$,
      route$
    ).pipe(
      tap(
        () => this._stateManager.markForCheck(segment),
        res => {
          this.snackBar.open('Error Occured', 'Dismiss', this.snackBarProps);
          this._log.log(res);
        }));

    this._cd.markForCheck();
  }

  createInfoWindow(s: Segment) {
    const infowindow = new SnazzyInfoWindow({
      marker: s.marker,
      content: 'Loading...',
      map: this._mapsService.gMap,
      closeWhenOthersOpen: true
    });

    const viewRef = this.vcf.createEmbeddedView(this.template, { $implicit: s });
    infowindow.setContent(viewRef.rootNodes[0]);
    viewRef.detectChanges();
    s.viewRef = viewRef;
    s.infoWindow = infowindow;
    return infowindow;
  }

  removeMarker(segment: Segment) {
    const segments = this._stateManager.select() as Segment[];
    let i = segments.indexOf(segment);
    i++;
    if (i > 0 && i < segments.length) {
      const nextSegment = segments[i];
      nextSegment.directionsRenderer.setDirections(null);
      nextSegment.directionsRenderer.setMap(null);
      i -= 2;
      if (i < segments.length) {
        const prevSegment = segments[i];
        this._mapsService.route(prevSegment.marker, nextSegment.marker).subscribe(result => {
          nextSegment.directionsRenderer = result;
          this._stateManager.reduceDistDur();
        });
      }
    }
    this._stateManager.removeSegment(segment);
    this._cd.markForCheck();
  }

  startAtCurrentLocation() {
    const updateFn = () => {
      this._stateManager.reduceDistDur();
      this._stateManager.updateLabels();
    };
    if (this.currentStart) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(result => {
          const marker = new google.maps.Marker({
            position: { lat: result.coords.latitude, lng: result.coords.longitude },
            animation: google.maps.Animation.DROP,
            label: '1',
            icon: mapMarker,
            map: this._mapsService.gMap
          });
          this._stateManager.addSegment(new Segment(marker), true);
          updateFn();
        });
      }
    } else {
      const segments = this._stateManager.select();
      const el = segments[0];
      const el2 = segments[1];
      this._stateManager.removeSegment(el);
      setInterval(() => {
        el.marker.setMap(null);
        el2.directionsRenderer.setDirections(null);
        el2.directionsRenderer.setMap(null);
        updateFn();
      }, 300);
    }
  }

  snapToRoad(polyline: google.maps.Polyline) {
    this._mapsService.snapToRoad(polyline).subscribe(points => {
      const line = new google.maps.Polyline({
        strokeColor: '#ff0099',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        path: points
      });

      line.setMap(this._mapsService.gMap);
      polyline.setMap(null);
    });
  }

  // updateRoutes() {
  //   const segments = this._stateManager.select() as Segment[];
  //   for (let i = 1; i < segments.length; i++) {
  //     const dest = segments[i];
  //     const orig = segments[i - 1];
  //     this._mapsService.route(orig.marker, dest.marker)
  //       .subscribe(result => dest.directionsRenderer = result);
  //   }
  // }

  changeTool(value) {
    switch (value) {
      case 'draw':
        this.drawingService.setDrawMode(google.maps.drawing.OverlayType.POLYLINE);
        break;
      case 'marker':
        this.drawingService.setDrawMode(google.maps.drawing.OverlayType.MARKER);
        break;
      case 'pan':
        this.drawingService.setDrawMode(null);
        break;
    }
    this._cd.markForCheck();
  }

  changeView(value) {
    switch (value) {
      case 'poi':
        this.darkTheme = true;
        this._mapsService.gMap.setOptions({ styles: mapStylesLight });
        setTimeout(() => {
          this.poi.showPOI();
          this.poi.displayPOI(this._mapsService.gMap.getBounds());
        }, 500); // hack to let map styles change before POIs are added
        break;
      case 'nav':
        this.darkTheme = false;
        this._mapsService.gMap.setOptions({ styles: null });
        this.poi.hidePOI();
        break;
    }
  }

  saveState() {
    this._stateManager.saveStateToLocalStorage();
    localStorage.setItem('last-map-center', JSON.stringify(this._mapsService.gMap.getCenter().toJSON()));
    localStorage.setItem('last-map-zoom', this._mapsService.gMap.getZoom().toString());
  }

  loadState() {
    this._stateManager.loadLocalStorageState();
    this._currentStart = false;
    const segs = this._stateManager.select() as Segment[];
    segs.forEach(s => this.createInfoWindow(s));
    if (segs.length > 1)
      this.centerOnSegments();
    else if (segs.length === 1)
      this._mapsService.gMap.setCenter(segs[0].marker.getPosition());
    else {
      const center = JSON.parse(localStorage.getItem('last-map-center'));
      const zoom = JSON.parse(localStorage.getItem('last-map-zoom'));
      if (center && zoom) {
        this._mapsService.gMap.setCenter(center);
        this._mapsService.gMap.setZoom(zoom);
      }
    }
  }

  private centerOnSegments() {
    if (this._stateManager.select('count') === 0) return;
    const bounds = this._stateManager.getAllBounds();
    this._mapsService.gMap.fitBounds(bounds, 50);
  }

  private addKbListeners() {
    this.subscriptions.add(
      fromEvent(window, 'keyup').subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 27)
          this.tool = 'pan';
        else if (e.ctrlKey && e.keyCode === 77) // ctrl + M
          this.tool = 'marker';
        else if (e.ctrlKey && e.keyCode === 68)  // ctrl + D
          this.tool = 'draw';
      }));
  }

  private gMapsEventToObservable(object: any, event: string) {
    return fromEventPattern(
      (handler: any) => object.addListener(event, handler),
      (handler: any, listener: any) => google.maps.event.removeListener(listener)
    );
  }

  public ToWords = (date) => distanceInWordsToNow(date);

  public showOverlay = (segment) => this.overlay.showSegment(segment);

  public clearAll($event: MatButtonToggleChange) {
    this._stateManager.clearState();
    $event.source.checked = false;
  }

  public logout = () => this._auth.logout();

  ngAfterViewInit() {
    this.initGMaps();
    this.loadState();
  }

  ngOnDestroy() {
    this.saveState();
    this.subscriptions.unsubscribe();
    this.drawingService.destroy();
    this._mapsService.clearMap();
  }

  @HostListener('window:unload', ['$event'])
  unloadHander() {
    this.saveState();
  }
}

