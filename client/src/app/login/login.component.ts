import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { LoggingService } from '../services/logging.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  animations: [
    trigger('signup', [
      state('true', style({
        height: '*'
      })),
      state('void', style({
        height: '0',
        opacity: '0'
      })),
      transition('void => true', animate('120ms ease-in')),
      transition('true => void', animate('120ms ease-out'))
    ])
  ]
})
export class LoginComponent implements OnInit {
  signup = false;
  email: string;
  name: string;
  pass: string;
  confirmPass: string;
  error: string;

  constructor(private _auth: AuthService, private _cd: ChangeDetectorRef, private _router: Router, private _log: LoggingService) { }

  ngOnInit() {

  }

  loginOrSignup() {
    let obs$;
    if (!this.signup) {
      obs$ = this._auth.authenticate(this.email, this.pass);
    } else {
      if (this.name && this.email && this.pass && this.pass === this.confirmPass)
        obs$ = this._auth.signup(this.name, this.email, this.pass);
    }

    obs$.subscribe(
      _ => this._router.navigateByUrl('/map'),
      res => {
        if (Array.isArray(res.error))
          this.error = res.error.join('\n');
        else if (typeof res.error === 'string')
          this.error = res.error;
        else {
          this.error = 'Unknown error occured';
          this._log.log(res);
        }
      }
    );
  }

  toggleSignUp() {
    this.signup = !this.signup;
  }
}
