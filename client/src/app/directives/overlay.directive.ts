import { Directive, ElementRef, AfterViewInit, Input } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { allSettled } from 'q';

@Directive({
  selector: '[appOverlay]'
})
export class OverlayDirective implements AfterViewInit {
  // tslint:disable-next-line:no-input-rename
  @Input('appOverlay') direction = 'top';
  nativeEl: HTMLElement;

  constructor(private elementRef: ElementRef) {
    this.nativeEl = this.elementRef.nativeElement;
  }

  ngAfterViewInit() {
    let classAnim: string;
    let overlayClass = '';
    this.nativeEl.classList.add('app-overlay');

    switch (this.direction) {
      case 'top':
        classAnim = 'translate-top';
        overlayClass = 'overlay-top';
        break;
      case 'right':
        classAnim = 'translate-right';
        overlayClass = 'overlay-right';
        break;
      case 'left':
        classAnim = 'translate-left';
        overlayClass = 'overlay-left';
        break;
      case 'bottom':
        classAnim = 'translate-bottom';
        overlayClass = 'overlay-bottom';
        break;
      default:
        break;
    }

    this.nativeEl.classList.add(overlayClass);

    // Hide overlays after view init.
    setTimeout(() => this.nativeEl.classList.add(classAnim), 5000);

    // fromEvent(window, 'mousemove').pipe(debounceTime(1500)).subscribe(e => this.nativeEl.classList.add(classAnim));
  }

}
