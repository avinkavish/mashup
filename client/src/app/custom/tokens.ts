import { InjectionToken } from '@angular/core';
import { PortalInjector } from '@angular/cdk/portal';

export const SEGMENT_DATA = new InjectionToken<{}>('SEGMENT_DATA');

