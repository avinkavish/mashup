import { PointOfInterest } from '../models/point-of-interest';

export class POIOverlay extends google.maps.OverlayView {
  public place: PointOfInterest;
  private viewRef: HTMLDivElement;
  private get position() {
    return new google.maps.LatLng(this.place.location.lat, this.place.location.lng);
  }
  constructor(place: any, map) {
    super();
    this.place = place;
    this.setMap(map);
  }

  /* Creates an overlay based on whether a thumbnail is available or not .
  *  Uses an icon if a thumbnail is not found
  */
  public onAdd() {
    const div = document.createElement('div');
    const img = document.createElement('img');
    const div_text = document.createElement('div');
    div_text.className = 'poi-text';
    div.style.opacity = '0';

    if (this.place.thumbnail_url) {
      div.className = 'poi-overlay with-thumbnail';
      img.src = this.place.thumbnail_url;
      div_text.innerHTML = this.place.perex || this.place.name;

    } else {
      div.className = 'poi-overlay with-icon';
      const color = this.colorByMarker(this.place.marker);
      div.style.backgroundColor = color[0];
      div.style.color = 'white';
      img.addEventListener('error', this.shortenImageURL);
      img.addEventListener('load', () => img.removeEventListener('error', this.shortenImageURL));
      img.src = `assets/${this.place.marker}.png`;
      div_text.innerText = this.place.name;
    }

    div.addEventListener('click', this.expandMap);
    div.appendChild(img);
    div.appendChild(div_text);
    this.viewRef = div;
    this.getPanes().overlayMouseTarget.appendChild(div);
    requestAnimationFrame(() => div.style.opacity = '1');
  }

  // The collection of thumbnails are incomplete but on each failed load, by reducing the specificity of the
  // category, we can arrive at an available thumbnail. Eg: If 'shoping:centre:mall' not available, then try 'shopping:centre'
  // These are one time calculations and as such should be persisted to the backend, this is a temp fix.
  shortenImageURL = ($event) => {
    const img = ($event.target as HTMLImageElement);
    const i = img.src.lastIndexOf(':');
    const url = img.src.substring(0, i);
      if (i === -1 || url === 'http') {
        img.removeEventListener('error', this.shortenImageURL);
        return;
      }
    img.src = img.src.substring(0, i) + '.png';
  }

  expandMap = () => {
    const map = this.getMap() as google.maps.Map;
    map.panTo(this.position);
    map.setZoom(14);
  }

  colorByMarker(marker: string): string[] {
    const s = marker.toLowerCase();
    if (s.includes('town'))
      return ['#7e57c2'];
    if (s.includes('island'))
      return ['#ff7043'];
    if (s.includes('gas_station'))
      return ['#0d47a1'];
    if (s.includes('traveling'))
      return ['#0d47a1'];
    if (s.includes('eating'))
      return ['#b71c1c'];
    if (s.includes('park'))
      return ['#388e3c'];
    if (s.includes('shopping'))
      return ['#af65d1'];

    return ['#388e3c'];
  }

  public draw() {
    const proj = this.getProjection();
    const p = proj.fromLatLngToDivPixel(this.position);
    this.viewRef.style.left = p.x + 'px';
    this.viewRef.style.top = `${p.y - 20}px`;
  }

  public onRemove() {
    this.viewRef.removeEventListener('click', this.expandMap);
    this.viewRef.parentNode.removeChild(this.viewRef);
    this.viewRef = null;
  }

}
