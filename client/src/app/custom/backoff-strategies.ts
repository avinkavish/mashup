import { mergeMap } from 'rxjs/operators';

import { throwError, timer } from 'rxjs';

/**
 * Sets up an exponential backoff strategy, i.e. 4, 8, 16, 32, 64, 128 secs before next retry
 * @param errorsToSkip - Error codes to skip when setting up strategy
 * Skips Internal Server Errors and Unauthorized Errors by default
 * @param timeout - in Seconds, if next wait period is greater than this value, retrys are abandoned
 */
export const exponentialTimer = (msg = '', errorsToSkip = [401, 400, 404], timeout = 600) =>
  (
    errors$ => errors$.pipe(mergeMap((err: any, i) => {
      const secs = Math.pow(2, i + 2);

      if (secs > timeout || errorsToSkip.includes(err.status))
        return throwError(err);

      console.log(`${msg}, retrying in ${secs} seconds...`);
      return timer(secs * 1000);
    }))
  );
