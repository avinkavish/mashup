import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SidebarService } from '../services/sidebar.service';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-map-drawer',
  templateUrl: './map-drawer.component.html',
  styleUrls: ['./map-drawer.component.css']
})
export class MapDrawerComponent implements OnInit, AfterViewInit {

  @ViewChild('sidebar') sideBar: MatSidenav;

  constructor(private sidebarService: SidebarService, private breakpointObserver: BreakpointObserver) { }

  ngOnInit() { }

  activateMobileLayout() { }

  ngAfterViewInit() {
    this.sidebarService.set(this.sideBar);

    this.breakpointObserver.observe([
      '(max-width:599px)'
    ]).subscribe(result => {
      console.log(result);
      if (result.matches)
        this.sideBar.mode = 'over';
      else
        this.sideBar.mode = 'side';
    });
  }
}
