import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { StateManager } from '../services/state.manager';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarComponent implements OnInit {
  public segments$;

  constructor(private stateManager: StateManager) {
    this.segments$ = stateManager.segments$;
   }

  ngOnInit() {
  }

  clearAll() {
    this.stateManager.clearState();
  }


}
