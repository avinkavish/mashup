import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, retryWhen, delayWhen, mergeMap, finalize, switchMap } from 'rxjs/operators';
import { timer, throwError, of, Observable } from 'rxjs';
import { exponentialTimer } from '../custom/backoff-strategies';

@Injectable({
  providedIn: 'root'
})
export class FlickrService {
  private API_KEY = 'noop';

  constructor(private http: HttpClient) { }

  public getByLocation(pos: google.maps.LatLngLiteral) {
    let obs$: Observable<any>;
    if (this.API_KEY === 'noop')
      obs$ = this.getFlickrKey();
    else
      obs$ = of(this.API_KEY);

    return obs$.pipe(switchMap(val =>
        this.http.get('https://api.flickr.com/services/rest/', {
          params: {
            method: 'flickr.photos.search',
            api_key: val,
            lat: pos.lat.toString(),
            lon: pos.lng.toString(),
            format: 'json',
            nojsoncallback: '1',
            radius: '8',
            accuracy: '13',
            content_type: '1',
            extras: 'tags',
            per_page: '10'
          }
        })),
      retryWhen(exponentialTimer()),
      map((r: any) => r.photos &&
        r.photos.photo &&
        r.photos.photo.map(p => `https://farm${p.farm}.staticflickr.com/${p.server}/${p.id}_${p.secret}_n.jpg`))
    );
  }

  public getFlickrKey() {
    return this.http.get<string>('api/config/flickr')
      .pipe(retryWhen(exponentialTimer('Failed to get flicker config')), tap(key => this.API_KEY = key));
  }

}

