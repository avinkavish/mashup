import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public get token() {
    return this._authUser && this._authUser.token;
  }

  private set authUser(user: any) {
    this._authUser = user;
    localStorage.setItem('user', JSON.stringify(user));
  }
  private _authUser;

  constructor(private _http: HttpClient, private _router: Router) {
    try {
      this._authUser = JSON.parse(localStorage.getItem('user') || null);
    } catch (error) {
      this._authUser = null;
    }
  }

  public authenticate(email: string, pass: string) {
    return this._http.post('/api/users/login', { email: email, password: pass }).pipe(tap((o: any) => this.authUser = o));
  }

  public signup(name: string, email: string, pass: string) {
    return this._http.post('/api/users/', { name: name, email: email, password: pass }).pipe(tap((o: any) => this.authUser = o));
  }

  logout() {
    localStorage.clear();
    this._router.navigateByUrl('/login');
  }
}
