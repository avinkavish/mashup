import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map } from 'rxjs/operators';
import { POIOverlay } from '../custom/poi-overlay';
import { differenceWith } from 'lodash';
import { of } from 'rxjs';
import { MapsService } from './maps.service';

@Injectable({
  providedIn: 'root'
})
export class POIService {
  private _displayedOverlays: POIOverlay[] = [];
  private _displayedMarkers: google.maps.Marker[] = [];
  private nextPage = '';
  private _prevBounds: google.maps.LatLngBounds;
  _hidePOI = false;

  constructor(private http: HttpClient, private _mapService: MapsService) { }

  public displayPOI(bounds: google.maps.LatLngBounds) {
    if (!bounds || this._hidePOI || bounds.equals(this._prevBounds)) return of('noop');
    this._prevBounds = bounds;
    return this.sygicPOI(bounds);
  }

  public hidePOI() {
    this._displayedOverlays.forEach(s => s.setMap(null));
    this._hidePOI = true;
  }

  public showPOI() {
    this._displayedOverlays.forEach(s => s.setMap(this._mapService.gMap));
    this._hidePOI = false;
  }

  /* Get points of interest from sygic and add them to the map
   * The tap operator taps in to the stream of incoming data,
   * the code diffs the existing POI and only updates POIs that weren't present
   * already on the map
   */
  private sygicPOI(bounds: google.maps.LatLngBounds) {
    return this.http.get('/api/poi/sygic/nearby', {
      params: {
        bounds: bounds.toUrlValue(),
      }
    }).pipe(
      tap((places: any[]) => {
        const markersToRemove = differenceWith(this._displayedOverlays, places, (a, b) => a.place.id === b.id);
        const placesToMark = differenceWith(places, this._displayedOverlays, (a, b) => a.id === b.place.id);
        const markersToKeep = differenceWith(this._displayedOverlays, markersToRemove, (a, b) => a.place.id === b.place.id);
        markersToRemove.forEach(p => p.setMap(null));
        this._displayedOverlays = [...markersToKeep, ...placesToMark.map(p => new POIOverlay(p, this._mapService.gMap))];
      })
    );
  }

  /* Google POI
   * Hard to filter, expensive and not geared towards tourism
   * not in use currently
   */
  private googlePOI(bounds: google.maps.LatLngBounds) {
    return this.http.get('/api/poi/google/nearby').pipe(
      tap((res: any) => {
        const places: any[] = res.places;

        this._displayedMarkers = places.map(val => new google.maps.Marker({
          map: this._mapService.gMap,
          position: val.location,
          icon: { url: val.icon, scaledSize: new google.maps.Size(32, 32) } as google.maps.Icon
        }));
      }));
  }


}
