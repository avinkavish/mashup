import { TestBed, inject } from '@angular/core/testing';

import { POIService } from './poi.service';

describe('SygicService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [POIService]
    });
  });

  it('should be created', inject([POIService], (service: POIService) => {
    expect(service).toBeTruthy();
  }));
});
