import { TestBed, inject } from '@angular/core/testing';

import { StateManager } from './state.manager';

describe('StateManager', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [State.ManagerService]
    });
  });

  it('should be created', inject([State.ManagerService], (service: State.ManagerService) => {
    expect(service).toBeTruthy();
  }));
});
