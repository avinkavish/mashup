import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';
import { mapStyles, directionsOptions } from '../../styles/mapStyles';
import { mapStyleWhite } from '../../styles/mapStyleWhite';
import { intersection } from 'lodash';
import { mapStylesLight } from '../../styles/mapStylesLight';

export const TypesToPick = [
  'point_of_interest',
  'park',
  'ward',
  'locality',
  'political',
  'colloquial_area',
  'administrative_area_level_1',
  'administrative_area_level_2',
  'administrative_area_level_3',
  'administrative_area_level_4',
  'administrative_area_level_5',
  'country',
];

@Injectable({
  providedIn: 'root'
})
export class MapsService {
  private apiKey = 'AIzaSyD96cDpFyZw48CuoshUhpSUQa9SDTo_GGU';
  private directionsService: google.maps.DirectionsService;
  private _gMap: google.maps.Map;

  private get geoCoder() {
    return this._geoCoder || (this._geoCoder = new google.maps.Geocoder());
  }
  private _geoCoder: google.maps.Geocoder;

  get gMap() {
    if (!this._gMap) throw Error('Map accessed before init');
    return this._gMap;
  }

  constructor(private http: HttpClient) {
    this.directionsService = new google.maps.DirectionsService();
  }

  public route(orig: google.maps.Marker, dest: google.maps.Marker): Observable<google.maps.DirectionsRenderer> {
    return Observable.create((obs: Observer<google.maps.DirectionsRenderer>) => {
      this.directionsService.route({
        origin: orig.getPosition(),
        destination: dest.getPosition(),
        travelMode: google.maps.TravelMode.DRIVING,
        avoidTolls: true,
      },
        result => {
          const directionsDisplay = new google.maps.DirectionsRenderer({
            ...directionsOptions,
            directions: result,
            // draggable: true,
            map: this._gMap
          });
          obs.next(directionsDisplay);
        });
    });
  }

  public geocodePlace(marker: google.maps.Marker): Observable<string> {
    return Observable.create((obs: Observer<string>) => {
      this.geoCoder.geocode({ location: marker.getPosition() }, result => {
        obs.next(this.getBestMatch(result));
      });
    });
  }

  public snapToRoad(polyline: google.maps.Polyline): Observable<google.maps.LatLng[]> {
    const pathsUrl = polyline.getPath().getArray().map(ll => ll.toUrlValue()).join('|');

    return this.http.get('https://roads.googleapis.com/v1/snapToRoads', {
      params: {
        key: this.apiKey,
        interpolate: 'true',
        path: pathsUrl
      }
    }).pipe(
      map((data: any) => {
        if (!data.snappedPoints) return null;
        return data.snappedPoints.map(p => new google.maps.LatLng(p.location.latitude, p.location.longitude));
      })
    );
  }

  public initMap(elRef: Element) {
    return this._gMap = new google.maps.Map(elRef, {
      center: {
        lat: -27.47,
        lng: 153.02
      },
      zoom: 12,
      disableDefaultUI: true,
      zoomControl: false,
      gestureHandling: 'greedy',
      styles: mapStylesLight
    });
  }

  public clearMap() {
    this._gMap = null;
  }

  private getBestMatch(result: google.maps.GeocoderResult[]) {
    for (const address of result) {
      for (const comp of address.address_components) {
        if (intersection(comp.types, TypesToPick).length > 0)
          return comp.short_name;
      }
    }
  }
}
