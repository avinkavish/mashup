import { Injectable, Injector } from '@angular/core';
import { Segment } from '../models/segment';
import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { InfoComponent } from '../info/info.component';
import { SEGMENT_DATA } from '../custom/tokens';

@Injectable({
  providedIn: 'root'
})
export class OverlayService {
  overlayRef: any;

  constructor(private overlay: Overlay, private _injector: Injector) { }

  showSegment(segment: Segment) {
    this.overlayRef = this.overlay.create({
      hasBackdrop: true,
      panelClass: 'info-panel',
      backdropClass: 'info-backdrop'
    });
    this.overlayRef.backdropClick().subscribe(() => this.hideOverlay());
    const infoPortal = new ComponentPortal(InfoComponent, null, this.createInjector(segment));
    this.overlayRef.attach(infoPortal);
  }

  hideOverlay() {
    this.overlayRef.detach();
    this.overlayRef.dispose();
  }

  createInjector(dataToPass): PortalInjector {
    const injectorTokens = new WeakMap();
    injectorTokens.set(SEGMENT_DATA, dataToPass);
    return new PortalInjector(this._injector, injectorTokens);
  }

}
