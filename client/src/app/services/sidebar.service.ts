import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  private _sidebar: MatSidenav;

 public get opened() {
   if (this._sidebar)
    return this._sidebar.opened;
  else
    return false;
  }

  set(sideBar: MatSidenav) {
    this._sidebar = sideBar;
  }

  toggle() {
    if (!this._sidebar)
      throw new Error('Sidebar toggled before set');

    this._sidebar.toggle();
  }

  constructor() { }
}
