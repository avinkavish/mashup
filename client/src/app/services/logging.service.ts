import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {

  constructor(private http: HttpClient) { }

  public log(val: any) {
    if (environment.production) {
      this.http.post('/api/log/errors', { error: val, agent: navigator.userAgent });
    } else
      console.log(val);
  }
}
