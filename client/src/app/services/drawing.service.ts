import { Injectable } from '@angular/core';
import { mapMarker } from '../../styles/mapStyles';

@Injectable({
  providedIn: 'root'
})
export class DrawingService {
  private _manager: google.maps.drawing.DrawingManager;

  constructor() { }

  public init(gMap: google.maps.Map) {
    return this._manager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: false,
      markerOptions: {
        animation: google.maps.Animation.DROP,
        position: { lat: -27.47, lng: 153.02 },
        icon: mapMarker,
        label: ''
      },
      map: gMap
    });
  }

  public setDrawMode(mode) {
    this._manager.setDrawingMode(mode);
  }

  destroy() {
    this._manager.setMap(null);
    this._manager.unbindAll();
    this._manager = null;
  }
}
