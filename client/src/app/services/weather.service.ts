import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, retryWhen } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { WeatherData } from '../models/segment';
import { exponentialTimer } from '../custom/backoff-strategies';
@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(private http: HttpClient) { }

  get(point: google.maps.LatLng) {
    return this.http.get<WeatherData>('/api/weather', {
      params: {
        lat: point.lat().toString(),
        lng: point.lng().toString()
      }
    }).pipe(retryWhen(exponentialTimer()));
  }
}
