import { Injectable } from '@angular/core';
import { Segment } from '../models/segment';
import { mapMarker, directionsOptions } from '../../styles/mapStyles';
import { MapsService } from './maps.service';
import { Subject } from 'rxjs';

/* State Management Service */
@Injectable({
  providedIn: 'root'
})
export class StateManager {

  /* Everytime an array of segments are set, it is emitted */
  private set segments(val) {
    this._segments = val;
    this.segments$.next(this._segments);
  }
  private _segments: Segment[] = [];

  public segments$ = new Subject<Segment[]>();

  constructor(private mapService: MapsService) { }

  loadLocalStorageState() {
    const stateStr = localStorage.getItem('trip-state') || '[]';
    const segmentJSON = JSON.parse(stateStr);
    if (segmentJSON.length === 0) {
      this.segments = [];
      return;
    }

    const segments = segmentJSON.map((val, index) => {
      const s = Segment.fromJSON(val);
      s.marker.setIcon(mapMarker);
      s.marker.setMap(this.mapService.gMap);
      s.marker.setLabel((index + 1).toString());
      if (s.directionsRenderer) {
        s.directionsRenderer.setOptions(directionsOptions);
        s.directionsRenderer.setMap(this.mapService.gMap);
      }
      return s;
    });
    this.segments = segments;
  }

  saveStateToLocalStorage() {
    localStorage.setItem('trip-state', JSON.stringify(this._segments));
  }

  clearState() {
    localStorage.removeItem('trip-state');
    this._segments.forEach(s => s.destroy());
    this.segments = [];
  }

  /* Any view can call this method to observe changes to segments */
  observeSegments() {
    return this.segments$.asObservable();
  }

  /* Views that require direct access to state can select any part of it */
  select(criteria = null) {
    if (!criteria)
      return this._segments;

    switch (criteria) {
      case 'count':
        return this._segments.length;
    }

    return null;
  }

  addSegment(segment: Segment, addToStart = false) {
    if (addToStart)
      this.segments = [segment, ...this._segments];
    else
      this.segments = [...this._segments, segment];
  }

  removeSegment(segment: Segment) {
    this.segments = this._segments.filter(s => s.marker.getPosition() !== segment.marker.getPosition());
    segment.destroy();
  }

  updateLabels() {
    for (let i = 1; i < this._segments.length; i++) {
      const el = this._segments[i];
      el.marker.setLabel(i.toString());
    }
  }

  reduceDistDur() {
    let accD = 0;
    let accT = 0;
    this._segments.forEach(s => {
      accD += s.getDistance();
      accT += s.getDuration();
      s.totalDistance = accD;
      s.totalDuration = accT;
      s.viewRef.markForCheck();
    });
  }

  getAllBounds() {
    const bounds = new google.maps.LatLngBounds();
    this._segments.forEach(s => bounds.extend(s.marker.getPosition()));
    return bounds;
  }

  /* Informs angular that the view is "dirty" and needs to checked for changes */
  markForCheck(segment: Segment) {
    segment.viewRef.markForCheck();
    this.segments = this._segments;
  }

}
