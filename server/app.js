const isProduction = process.env.NODE_ENV === "production";

const express = require('express');
const mongoose = require('mongoose');
const errorHandler = require('errorhandler');
const passport = require('passport');

mongoose.Promise = global.Promise;

const app = express();

app.use(require('morgan')('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

if (!isProduction) {
  app.use(express.static('wwwroot'));
  app.use(errorHandler());
}
const db_url = isProduction ? 'mongodb://db/mashup' : 'mongodb://localhost/mashup';
const connectWithRetry = () =>
  mongoose.connect(db_url, { useNewUrlParser: true }).catch(_ => {
    console.log('MongoDb connection failed... retrying in 5 secs');
    setTimeout(connectWithRetry, 5000);
  });
connectWithRetry();
mongoose.set('debug', !isProduction);

// Models
require('./models/User');
require('./config/passport');

// Routes
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(require('./routes'));
app.use(passport.initialize());

if (!isProduction) {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.listen(3000, () => console.log('listening on port 3000'));
