var path = require('path');

module.exports = {
  EXPIRY_DAYS: 30,
  SECRET: 'L9ZjT$4@=--f+KbJ',
  PATH: path.join(__dirname, '..', 'wwwroot', 'images')
}
