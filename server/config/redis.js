const isProduction = process.env.NODE_ENV === "production";
const redis = require('redis')
require('bluebird').promisifyAll(redis);
module.exports = redis.createClient({
host: isProduction ? 'redis' : 'localhost'
});