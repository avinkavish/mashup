const mongoose = require('mongoose');
const passport = require('passport');
const strat = require('passport-json').Strategy;

const User = mongoose.model('User');

passport.use(new strat({
  usernameProp: 'email',
  passwordProp: 'password',
  passReqToCallback: false,
  session: false
}, (email, password, done) => { 
  User.findOne({ email: email }).then(u => { 
    if (!u || !u.checkPassword(password))
      return done(null, false, [ 'email or password is invalid' ]);
      
    return done(null, u);
  }).catch(done);
}));