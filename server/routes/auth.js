const SECRET = require('../config/constants').SECRET;
const jwt = require('express-jwt');

const getTokenFromHeader = req => {
  const { headers: { authorization } } = req;

  if (!authorization)
    return null;

  splitHeader = authorization.split(' ');
  if (splitHeader[0].toLowerCase() === 'bearer') {
    return splitHeader[1];
  }

  return null;
};

const auth = {
  required: jwt({
    secret: SECRET,
    userProperty: 'user',
    getToken: getTokenFromHeader,
  }),
  optional: jwt({
    secret: SECRET,
    userProperty: 'user',
    getToken: getTokenFromHeader,
    credentialsRequired: false,
  }),
};

module.exports = auth;