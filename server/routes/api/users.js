const mongoose = require('mongoose');
const passport = require('passport');
const auth = require('../auth');

const router = require('express').Router();
const User = mongoose.model('User');

router.post('/', auth.optional, (req, res, next) => {
  const body = req.body;

  if (!body.email)
    return res.status(422).json(['email is required']);

  if (!body.password)
    return res.status(422).json(['password is required']);

  const user = new User(body);
  user.setPassword(body.password);

  return user.save().then(() => res.json(user.authJSON()));
});

router.post('/login', auth.optional, (req, res, next) => {
  const { body: { email, password } } = req;

  if (!email)
    return res.status(422).json(['email is required']);

  if (!password)
    return res.status(422).json(['password is required']);

  return passport.authenticate('json', { session: false }, (err, user, info) => {
    if (err)
      return next(err);

    if (user) {
      user.token = user.makeJWT();
      return res.json(user.authJSON());
    }
    return res.status(400).json(info).end();
  })(req, res, next);
});

router.get('/me', auth.required, (req, res, next) => {
  return User.findById(req.user.id).then(user => {
    if (!user)
      return res.sendStatus(400);

    return res.json(user.authJSON());
  });
});

module.exports = router;