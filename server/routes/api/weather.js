const express = require('express');
const router = express.Router();
const redisClient = require('../../config/redis');
const httpClient = require('axios');

// Meters, weather is cached at a granularity of 5km
const searchRadius = 5000;
const weatherKey = '009056b625a25c9a862c56eca177775d';

router.get('/', TryRedisCache, GetOpenWeather, CacheAndDispatch);

async function TryRedisCache(req, res, next) {
  if (!req.query.lat || !req.query.lng) return res.status(400).end();

  const latTrunc = +(Number(req.query.lat).toFixed(3));
  const lngTrunc = +(Number(req.query.lng).toFixed(3));
  req._lat = latTrunc;
  req._lng = lngTrunc;

  const cachedForecast = await redisClient.georadiusAsync('weather:forecast:5day', lngTrunc, latTrunc, searchRadius * 10, 'm');
  if (!cachedForecast || cachedForecast.length < 1) return next();
  req._forecast = JSON.parse(cachedForecast[0]);
  console.log('Cache hit. Weather forecast ', latTrunc, lngTrunc);
  const cachedWeather = await redisClient.georadiusAsync('weather:current', lngTrunc, latTrunc, searchRadius, 'm');
  if (!cachedWeather || cachedWeather.length < 1) return next();
  req._current = JSON.parse(cachedWeather[0]);
  console.log('Cache hit. Current Weather ', latTrunc, lngTrunc);
  return res.json({ current: req._current, forecast: req._forecast });
}

async function GetOpenWeather(req, res, next) {
  let forecastTask, weatherTask;
  if (!req._forecast)
    forecastTask = httpClient.get('http://api.openweathermap.org/data/2.5/forecast', {
      params: {
        units: 'metric',
        APPID: weatherKey,
        lat: req._lat,
        lon: req._lng
      }
    });
  if (!req._current)
    weatherTask = httpClient.get('http://api.openweathermap.org/data/2.5/weather', {
      params: {
        units: 'metric',
        APPID: weatherKey,
        lat: req._lat,
        lon: req._lng
      }
    });

  if (forecastTask) {
    const forecast = await forecastTask;
    req._forecast = forecast.data.list.map(v => ({
      timestamp: v.dt,
      temp: v.main.temp,
      desc: v.weather[0].main,
      icon: 'http://openweathermap.org/img/w/' + v.weather[0].icon + '.png'
    }));
  }
  if (weatherTask) {
    const weather = await weatherTask;
    req._current = {
      temp: weather.data.main.temp,
      desc: weather.data.weather[0].description,
      icon: 'http://openweathermap.org/img/w/' + weather.data.weather[0].icon + '.png'
    };
  }
  return next();
}

async function CacheAndDispatch(req, res) {
  const f = JSON.stringify(req._forecast);
  const c = JSON.stringify(req._current);
  redisClient.geoadd('weather:forecast:5day', req._lng, req._lat, f);
  redisClient.geoadd('weather:current', req._lng, req._lat, c);

  return res.json({ forecast: req._forecast, current: req._current });
}

module.exports = router;