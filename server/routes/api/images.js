const { promisify } = require('util');
const mongoose = require('mongoose');
const formidable = require('formidable');
const router = require('express').Router();
const fs = require('fs');
const uuid = require('uuid/v4');
const auth = require('../auth');
const AWS = require('aws-sdk');
const upload_path = require('../../config/constants').PATH;
const redis = require("redis");

const bucket = 'avin.mashup.images';
const readFileAsync = promisify(fs.readFile);
const User = mongoose.model('User');

var s3 = new AWS.S3({
  region: 'ap-southeast-2'
});

router.post('/upload', auth.required, async (req, res) => {
  const form = new formidable.IncomingForm();

  form.parse(req);
  const image = {};
  form.on('field', (name, value) => {
    image[name] = value;
  });
  form.on('fileBegin', (name, file) => {
    file.path = upload_path + '/' + file.name;
  });
  let filePath;
  form.on('file', (name, file) => {
    filePath = file.path;
  });

  form.on('end', async () => {
    const fileBits = await readFileAsync(filePath);
    const key = uuid() + '.jpeg';
    try {
      const data = await s3.putObject({
        Bucket: bucket,
        Key: key,
        Body: fileBits,
      }).promise();
    } catch (error) {
      console.log('S3 upload failed\n' + err);
      res.status(500).json({ message: 'Upload to S3 failed' });
    }
    console.log('Uploaded to s3');
    image.s3Key = key;

    const updateResult = await User.findByIdAndUpdate(req.user.id, { $push: { images: image } });
    res.status(200).json({ 
      message: 'Upload succesful', 
      url: s3.getSignedUrl('getObject', { Bucket: bucket, Key: key }) 
    });
  });
});

router.get('/', auth.required, async (req, res) => {
  const user = await User.findById(req.user.id, { images: 1 }, { lean: true });
  const params = {Bucket: bucket };
  user.images.forEach(el => {
        params.Key = el.s3Key;
        el.url = s3.getSignedUrl('getObject', params);
  });
    res.json(user.images);
});

module.exports = router;