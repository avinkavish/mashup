const router = require('express').Router();
const httpClient = require('axios');
const redisClient = require('../../../config/redis');
const turf = require('@turf/turf');
const uuid = require('uuid').v4;
const _ = require('lodash');

apiKey = 'sLqxA4GjNSIu6FQne49o9SBzf8RPA8u327vbuQy9';
apiKey2 = 'vfHkUnCG9j3oRZTBNT52U1aOAEE1GKj57draZ0rw';

router.get('/nearby', tryRedisCache, trySygicAPI);

async function tryRedisCache(req, res, next) {
   if (!req.query.bounds) return res.status(401).end();

  const bounds = req.query.bounds.split(",").map(s => +(Number(s).toFixed(3)));
  const sw = turf.point(bounds.slice(0, 2));
  const ne = turf.point(bounds.slice(2));
  const dist = +(turf.distance(sw, ne, { units: 'meters' }).toFixed(0));
  // console.log(bounds);
  // console.log(dist);
  req._bounds = bounds;
  req._dist = dist;

  // Map is not consumable at this size
  if (dist < 250) return res.status(400).json({ errors: ["Bounds are less than 250m apart"] }).end();

  try {
    let resultsSE = await redisClient.georadiusAsync('poi:sygic', bounds[1], bounds[0], (dist * 0.1), 'm');
    let resultsNE = await redisClient.georadiusAsync('poi:sygic', bounds[3], bounds[2], (dist * 0.1), 'm');

    if (!resultsNE || !resultsSE) return next();

    const uuidSE = resultsSE.map(v => v.substring(3));
    const uuidNE = resultsNE.map(v => v.substring(3));
    const cacheHits = _.intersection(uuidSE, uuidNE);

    if (!cacheHits || cacheHits.length < 1) return next();
    const placesStr = await redisClient.getAsync(cacheHits[0]);
    console.log('Cache hit. Key: ' + cacheHits[0] + ' Bounds: ' + bounds);
    res.set('Content-Type', 'application/json');
    return res.send(placesStr);

  } catch (error) {
    console.log(error);
    res.status(500).end();
  }
}

async function trySygicAPI(req, res) {
  try {
    
    const result = await httpClient.get('https://api.sygictravelapi.com/1.1/en/places/list', {
      headers: { 'x-api-key': apiKey },
      params: {
        bounds: req.query.bounds,
        limit: '40',
      }
    });
    const places = result.data.data.places;
    // console.log(places);
    const cacheKey = uuid();
    
    redisClient.set(cacheKey, JSON.stringify(places));
    await redisClient.geoadd('poi:sygic', req._bounds[1], req._bounds[0], 'sw:' + cacheKey);
    await redisClient.geoadd('poi:sygic', req._bounds[3], req._bounds[2], 'ne:' + cacheKey);
    return res.json(places);
    
  } catch (error) {
    console.log(error);
    res.status(500).end();
  }
}

module.exports = router;

