const router = require('express').Router();
const auth = require('../../auth');
const httpClient = require('axios');

apiKey = 'AIzaSyAfFS2TVhYsunXLpPxkopzhzEuVRX2Hcoo';
types = [
  'amusement_park',
  'aquarium',
  'art_gallery',
  'bar',
  'cafe',
  'campground',
  'casino',
  'hindu_temple',
  'movie_theater',
  'museum',
  'night_club',
  'park',
  'pet_store',
  'restaurant',
  'rv_park',
  'shopping_mall',
  'spa',
  'stadium',
  'zoo'
];

const googleMapsClient = require('@google/maps').createClient({
  key: apiKey,
  Promise: Promise
});

router.get('/nearby', auth.optional, async (req, res) => {
  const { param } = req;
  //console.log(param);
  try {
    let result = await googleMapsClient.placesNearby({
      location: '-27.47,153.02',
      radius: 50000,
    }).asPromise();

    let output = { places: mapNearbyResult(result), nextPage: result.json.next_page_token };

    res.json(output).end();
  } catch (error) {
    console.log(error);
    res.json(error);
  }

});

router.get('/search', auth.optional, async (req, res) => {
  const { param } = req;
  //console.log(param);

  try {
    const result = await httpClient.get('https://maps.googleapis.com/maps/api/place/findplacefromtext/json', {
      params:
      {
        key: apiKey,
        input: encodeURIComponent('hotel'),
        inputtype: 'textquery',
        locationbias: 'circle:50000@-27.47,153.02',
        fields: 'icon,name,photos,types'
      }
    });
    res.json(result.data).end();
  } catch (error) {
    console.log(error);
    res.json('error');
  }

});


function mapNearbyResult(result) {
  return result.json.results.map(v => ({
    icon: v.icon,
    id: v.id,
    location: v.geometry.location,
    name: v.name,
    photo: { ref: v.photos[0].photo_reference, attributions: v.photos[0].html_attributions }
  }));
}


module.exports = router;
