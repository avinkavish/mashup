const router = require('express').Router();

router.use('/google', require('./google'));
router.use('/sygic', require('./sygic'));

module.exports = router;
