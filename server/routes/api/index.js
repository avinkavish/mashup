const express = require('express');
const router = express.Router();
const auth = require('../auth');

router.use('/users', require('./users'));
router.use('/images', auth.required, require('./images'));
router.use('/poi', auth.required, require('./poi'));
router.use('/weather', auth.required, require('./weather'));
router.use('/config', auth.required, require('./config'));


module.exports = router;

