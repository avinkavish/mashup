const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const { EXPIRY_DAYS, SECRET } = require('../config/constants');

const UserSchema = new mongoose.Schema({ 
  email: { type: String, unique: true },
  name: String,
  hash: String, 
  salt: String }, 
  {timestamps: true, strict: false});

UserSchema.methods.setPassword = function(pass) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(pass, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.checkPassword = function(pass) {
  return (this.hash === crypto.pbkdf2Sync(pass, this.salt, 10000, 512, 'sha512').toString('hex'));
};

UserSchema.methods.makeJWT = function() {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + EXPIRY_DAYS);
  return jwt.sign({ email: this.email, id: this._id, exp: (expiry.getTime() / 1000)}, SECRET);
}

UserSchema.methods.authJSON = function() {
  return {
    _id: this._id,
    email: this.email,
    token: this.makeJWT(),
  };
};

mongoose.model('User', UserSchema);

