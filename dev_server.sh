#!/bin/bash
sudo service mongod start & redis-server &
cd ./server
nodemon app.js &
cd ../client
ng serve --hmr -o
